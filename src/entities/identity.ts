import { IClaim } from '../interfaces/iClaim';
import { IIdentity } from '../interfaces/iIdentity';

export class Identity implements IIdentity 
{
    private _claims: Partial<IClaim>[]
    private _claimsTable: Map<String, Partial<IClaim>>;
    id: string;
    issuer: string;
    name: string;
    email: string;
    
    get claims(): Partial<IClaim>[]
    {
        return this._claims;
    }

    set claims(value: Partial<IClaim>[])
    {
        this._claims = value;
        
        this._claimsTable.clear();

        for(let claim of this._claims)
        {
            let scopes = Array.isArray(claim.scope) ? claim.scope : [claim.scope];

            for(let scope of scopes)
            {
                this._claimsTable.set(this.getClaimId(claim.type, claim.value, scope), claim);
            }

        }
    }

    getClaimId(type: string, value: string, scope: string): string
    {
        return `${scope || ''}::${type || ''}::${value || ''}`;
    }

    constructor(identity?: Partial<IIdentity>)
    {
        this._claims = [];
        this._claimsTable = new Map();

        this.id = identity?.id;
        this.claims = identity?.claims || [];
        this.issuer = identity?.issuer;
        this.name = identity?.name;
        this.email = identity?.email;
    }

    hasClaim(type: string, valueOrValues: string | string[], scopeOrScopes?: string | string[]): boolean
    {
        if(!this.claims) return false;

        let values = Array.isArray(valueOrValues) ? [...new Set(valueOrValues)] : [valueOrValues];

        let scopes = Array.isArray(scopeOrScopes) ? [...new Set(scopeOrScopes)] : [scopeOrScopes];

        for(let value of values)
        {
            for(let scope of scopes)
            {
                if(this._claimsTable.has(this.getClaimId(type, value, scope)))
                {
                    return true;
                }
            }
        }

        return false;
    }

    getClaim(type: string, value: string, scope?: string): Partial<IClaim>
    {
        if(!this._claimsTable.has(this.getClaimId(type, value, scope))) return undefined;

        return this._claimsTable.get(this.getClaimId(type, value, scope));
    }

    getClaims(type: string, value: string, scope?: string): IClaim[]
    {
        if(!this.claims) return [];

        let result = [];

        let matchingTypeValueClaims = this.claims.filter((c) => c.type === type && c.value === value);

        for(let claim of matchingTypeValueClaims)
        {
            if(claim.scope === scope) 
            {
                result.push(claim);
                continue;
            }

            //For arrays
            let scopes = Array.isArray(claim.scope) ? claim.scope : [claim.scope];

            if(scopes.includes(scope))
            {
                result.push(claim);
            }
        }

        return result;
    }
}