import { EventEmitter } from 'events';
import { IEvent } from '../interfaces/iEvent';

import { IPolicy } from '../interfaces/iPolicy';
import { IRoute } from '../interfaces/iRoute';
import { LogLevel } from '../interfaces/logLevel';
import { Result } from '../interfaces/result';
import { performance } from 'perf_hooks';

export class Route extends EventEmitter implements IRoute 
{
    name: string;
    when: string;
    enabled: boolean;
    policies: IPolicy[];
    logLevel: LogLevel;

    constructor(route?: Partial<IRoute>) 
    {
        super();

        this.name = route?.name || '';
        this.enabled = route?.enabled || true;
        this.policies = route?.policies || [];
        this.logLevel = route?.logLevel;
        this.when = route?.when;
    }

    async process(event: IEvent): Promise<void>
    {
        for(let policy of this.policies)
        {
            let startTime = performance.now();

            try
            {
                const result: Result =  await policy.process(event);
                
                if(result === Result.Completed)
                {
                    break;
                }
            }
            catch(error)
            {
                throw error;
            }
            finally
            {
                let endTime = performance.now();

                if(LogLevel.gte(policy.logLevel, LogLevel.Trace)) 
                {
                    this.emit(LogLevel.Trace, `${event.id} - ${event.correlationId} - Executed ${policy.name} policy [${endTime - startTime}ms]`);
                }
            }
        }
    }
}