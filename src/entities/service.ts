import { EventEmitter } from 'events';
import { existsSync } from 'fs';
import oneliner = require('one-liner');
import { performance } from 'perf_hooks';

import { IConfig } from '../interfaces/iConfig';
import { IEvent } from '../interfaces/iEvent';
import { IPolicyHandlerLoader } from '../interfaces/iPolicyHandlerLoader';
import { IService } from '../interfaces/iService';
import { LogLevel } from '../interfaces/logLevel';
import { ErrorResponse } from './errorResponse';
import { PolicyHandlerLoader } from './policyHandlerLoader';

export class Service extends EventEmitter implements IService 
{
    isReady: boolean = false;
    onStartPromise: Promise<boolean>;

    constructor(private config: IConfig, private loader: IPolicyHandlerLoader = new PolicyHandlerLoader())
    {
        super();

        for(let module of this.config.modules)
        {
            this.loader.load(module);
        }

        let customHandlerPath = 'handlers';

        if(existsSync(customHandlerPath))
        {
            this.loader.loadCustomHandlers(customHandlerPath);
        }
        else
        {
            this.emit(LogLevel.Trace, `configurapi -  - Warning. Custom handlers directory does not exist.`);
        }

        for( let route of this.config.events.values()){
                for(let policy of route.policies)
                {
                    policy.handler = loader.get(policy.name);
                    policy.logLevel = policy.logLevel || route.logLevel;
                    this.configureLogs(policy);
                }
                this.configureLogs(route);
                this.emit(LogLevel.Trace, `configurapi -  - Registerd ${route.name} route.`);
    }
    }

    private initDone: boolean;
    async init()
    {
        this.onStartPromise ??= this._handleEvent('on_start');
        await this.onStartPromise
        this.initDone = true;
    }

    configureLogs(svc: EventEmitter & { logLevel: LogLevel}): void 
    {
        for(const level of [ LogLevel.Trace, LogLevel.Debug, LogLevel.Error ])
        {
            if(LogLevel.gte(svc.logLevel, level)) svc.on(level, (msg: string) => this.emit(level, oneliner(msg)));
        }
    }

    async process(event: IEvent): Promise<void>
    {
        if (!this.initDone)
        {
            await this.init();
        }

        if (!this.isReady)
        {
            try
            {
                await this._handleEvent('on_first_request', event);
            }
            catch(error)
            {
                try
                {
                    await this._handleError(event, error);
                }
                finally
                {
                    throw error;
                }
            }

            this.isReady = true;
        }

        let startTime = performance.now();

        try
        {
            await this._handleEvent('on_before_request', event);
            
            if(await this._handleEvent(event.name, event) || await this._handleEvent(event.method, event))
            {
                //Great
            }
            else if(await this._handleEvent('catchall', event))
            {
                //Great
            }
            else
            {
                throw new ErrorResponse('Not Implemented.', 501, `Route for '${event.name}' event could not be found.'`);
            }

            if(event.response && event.response instanceof ErrorResponse)
            {
                throw event.response;
            }

            await this._handleEvent('on_success', event);
        }
        catch(error)
        {
            await this._handleError(event, error);
        }
        finally
        {
            try
            {
                await this._handleEvent('on_after_request', event);
            }
            catch(error)
            {
                 await this._handleError(event, error);
            }
            
            let endTime = performance.now();

            let route = this.config.events.get(event.name);
            
            if(route)
            {
                if(LogLevel.gte(route.logLevel, LogLevel.Trace))
                {
                    this.emit(LogLevel.Trace, `${event.id} - ${event.correlationId} - Handled ${event.name} [${endTime - startTime}ms]`);
                }
            }
        }
    }

    async _handleEvent(eventName: string, event?: IEvent): Promise<boolean>
    {
        let result = false;
        if(this.config.events.has(eventName))
        {
            await this.config.events.get(eventName).process(event);
            result = true;
        }

        return result;
    }

    async _handleError(event: IEvent, error: object): Promise<void>
    {
        if(error instanceof Error)
        {
            this.emit(LogLevel.Error,  `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${error.message}`);

            event.response = new ErrorResponse(error, error['statusCode'] || 500);
        }
        else if(error instanceof ErrorResponse || ErrorResponse.isErrorResponse(error))
        {
            this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${(error as ErrorResponse).error}`);
            
            event.response = error;
        }
        else
        {
            this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${error === undefined ? 'undefined' : error.toString()}`);

            event.response = new ErrorResponse("Internal Server Error", 500, error.toString());
        }

        if(this.config.events.has('on_error'))
        {
            try
            {
                this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - Failed to handle ${event.name}: ${error}`);

                await this._handleEvent('on_error', event);
            }
            catch(reason)
            {
                this.emit(LogLevel.Error, `${event.id} - ${event.correlationId} - ${reason}`);
            }
            return;
        }
    }
}