export interface IClaim 
{
    type: string;
    value: string;
    scope: string | string[];

    //A helper property to return all scopes in an array.
    readonly scopes: string[];
}