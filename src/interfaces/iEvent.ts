import { IIdentity } from "./iIdentity";
import { IRequest } from "./iRequest";
import { IResponse } from "./iResponse";

export interface IEvent
{
    id: string;
    correlationId: string;
    params: Record<string, any>;
    name: string;
    request: Partial<IRequest>;
    response: Partial<IResponse>;
    method: string;
    version: string;
    identity: IIdentity;
    payload: Record<string, any> | string | undefined;
    
    resolve(str:string): string;
}