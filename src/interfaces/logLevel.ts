export enum LogLevel {
    Debug = 'debug', 
    Trace = 'trace',
    Info = 'info',
    Warn = 'warn',
    Error = 'error',
    None = 'none'
}
export namespace LogLevel
 {
    const LogLevelRank: { [key: string]: number } = 
    {
        [LogLevel.Debug]: 100,
        [LogLevel.Trace]: 75,
        [LogLevel.Info]: 25,
        [LogLevel.Warn]: 50,
        [LogLevel.Error]: 10,
        [LogLevel.None]: 1
    };

    export function gte(l1: LogLevel, l2: LogLevel): boolean 
    {
        return (LogLevelRank[l1?.toString()] || 0) >= (LogLevelRank[l2?.toString()] || 0);
    }
}