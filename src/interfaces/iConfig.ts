import { IRoute } from './iRoute';

export interface IConfig 
{
    modules: string[];
    events: Map<String, IRoute>;
}