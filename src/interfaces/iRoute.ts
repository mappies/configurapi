import { EventEmitter } from 'events';
import { IEvent } from './iEvent';
import { IPolicy } from './iPolicy';
import { LogLevel } from './logLevel';

export interface IRoute extends EventEmitter
{
    name: string;
    when: string;
    enabled: boolean;
    policies: IPolicy[];
    logLevel: LogLevel;

    process(event: IEvent): Promise<void>;
}