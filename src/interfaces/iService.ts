import { IEvent } from "./iEvent";
import { IServiceListener } from "./iServiceListener";

export interface IService 
{
    /**
     * Call to trigger on_start event after instantiating a new instance
     */
    init(): Promise<void>;
    on(type:string, listener:IServiceListener): void;
    process(event:IEvent):Promise<void>;
}