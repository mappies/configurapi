import { assert } from 'chai';
import { ErrorResponse } from '../src/entities/errorResponse';
import deepEqual from 'deep-equal';

describe('ErrorResponse', () =>  
{
    describe("Constructor", () => 
    {
        it('Accept status code and details', () => 
        {
            let response = new ErrorResponse("hi", 200, "world");
            assert.isTrue(deepEqual({'statusCode':200, 'message': 'hi', 'details': 'world'}, response.body));

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.equal('string', typeof response['error']);
            assert.equal('hi', response['error']);
        });
        it('Accept string', () => 
        {
            let response = new ErrorResponse("hi");
            assert.isTrue(deepEqual({'statusCode':500, 'message': 'hi', 'details': ''}, response.body));

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.equal('string', typeof response['error']);
            assert.equal('hi', response['error']);
        });
        
        it('Accept ErrorResponse', () => 
        {
            let response = new ErrorResponse(new ErrorResponse("hi", 500, "world"));
            assert.isTrue(deepEqual({'statusCode':500, 'message': 'hi', 'details': 'world'}, response.body));

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.isTrue(response['error'] instanceof ErrorResponse);
            assert.equal('hi', response['error']['message']);
        });

        it('Accept Error', () => 
        {
            let response = new ErrorResponse(new Error("hi"), 400);
            assert.equal(400, response.body.statusCode);
            assert.equal('hi', response.body.message);
            assert.isTrue(response.body.details['length'] > 0);

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.isTrue(response['error'] instanceof Error);
            assert.equal('hi', response['error']['message']);
            assert.isDefined(response['error']['stack']);
        });

        it('isErrorResponse - false - plain object', () => 
        {
            assert.isFalse(ErrorResponse.isErrorResponse({}))
        });
        
        it('isErrorResponse - true', () => 
        {
            assert.isTrue(ErrorResponse.isErrorResponse(new ErrorResponse(new Error("hi"), 400)))
        });
        
        it('isErrorResponse - true - plain object', () => 
        {
            assert.isTrue(ErrorResponse.isErrorResponse({statusCode:200, body:'' , message: '', details: '', error: '', extra: ''}));
        });
    });
});