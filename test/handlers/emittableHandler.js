const LogLevel = require('../../src/interfaces/logLevel');
module.exports = function emittableHandler(event)
{
    this.emit(LogLevel.Trace, "Hello from emittableHandler");
};