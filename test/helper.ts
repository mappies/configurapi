import { parse } from 'url';
import { IRequest } from '../src/interfaces/iRequest';

export function createRequest(method, path, payload = {}, headers = {}): Partial<IRequest> {
    let url = parse(path, true);
    return {
        method: method,
        query: url.query,
        params: {},
        payload: payload,
        path: url.pathname || undefined,
        headers: headers
    };
}